FROM python:latest

WORKDIR /code

COPY requirments.txt /code

RUN pip install -U pip
RUN pip install psycopg2
RUN pip install -r requirments.txt

COPY . /code/

EXPOSE 8000

CMD ["gunicorn", "config.wsgi", ":8000"]
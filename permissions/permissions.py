from django.contrib.auth.mixins import UserPassesTestMixin, AccessMixin

class IsSuperUserMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_authenticated and self.request.user.is_superuser


class IsNotAuthenticated(UserPassesTestMixin):
    def test_func(self):
        return not self.request.user.is_authenticated


class AdminGroupMixin(AccessMixin):
    permission_required = 'admin'
    permission_denied_message = 'you have not access'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.groups.filter(name='admin').exists():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)


class QuestionGroupMixin(AccessMixin):
    permission_required = 'admin'
    permission_denied_message = 'you have not access'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.groups.filter(name__in=['admin', 'view_question']).exists():
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)
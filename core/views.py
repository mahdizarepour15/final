from django.views import generic
from django.shortcuts import render
from permissions.permissions import QuestionGroupMixin
from . import models


class Home(generic.View):
    def get(self, request):
        return render(request, 'core/home.html')


class Question(QuestionGroupMixin, generic.ListView):
    model = models.Question
    context_object_name = 'questions'
    template_name = 'core/question.html'
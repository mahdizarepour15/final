from rest_framework import serializers
from . import models


class QuestionSerializer(serializers.ModelSerializer):
    """
        serializer qusetion and response that
        with all answers(to the another process use SerializerMethodField and one method startswith get_)
    """
    answers = serializers.SerializerMethodField()

    class Meta:
        model = models.Question
        fields = (
            'id',
            'user',
            'title',
            'slug',
            'body',
            'created',
            'last_edit',
            'answers',
        )

    def get_answers(self, obj):
        """this method for another process, obj=question instance"""
        result = obj.answers.all()
        return AnswerSerializer(instance=result, many=True).data


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Answer
        fields = (
            'id',
            'user',
            'question',
            'body',
            'created',
            'last_edit',
        )
from django.urls import path
from . import api_views


urlpatterns = [
    path('question/', api_views.QuestionListView.as_view(), name='question_list'),
    path('question/<int:pk>/', api_views.QuestionRetrieveView.as_view(), name='question_retrieve'),
    path('question/create/', api_views.QuestionCreateView.as_view(), name='question_create'),
    path('question/update/<int:pk>/', api_views.QuestionUpdateView.as_view(), name='question_update'),
    path('question/delete/<int:pk>/', api_views.QuestionDeleteView.as_view(), name='question_delete'),
    path('answer/<int:pk>/', api_views.AnswerListView.as_view(), name='answer_list'),
    path('answer/create/<int:pk>/', api_views.AnswerCreateView.as_view(), name='answer_create'),
    path('answer/update/<int:pk>/', api_views.AnswerUpdateView.as_view(), name='answer_update'),
    path('answer/delete/<int:pk>/', api_views.AnswerDeleteView.as_view(), name='answer_delete'),
]
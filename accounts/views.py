from django.views.generic import FormView, CreateView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import views as auth_view, login, authenticate
from django.urls import reverse_lazy
from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import get_user_model
from .models import CustomUser
from . import forms
from permissions.permissions import IsNotAuthenticated


class UserRegister(IsNotAuthenticated, FormView):
    model = CustomUser
    form_class = forms.UserRegistrationForm
    template_name = 'accounts/register.html'
    success_url = reverse_lazy('accounts:dashboard')

    def form_valid(self, form):
        cd = form.cleaned_data
        user = CustomUser.objects.create_user(
            username=cd['username'],
            email=cd['email'],
            password=cd['password'],
        )
        return super().form_valid(form)


class UserLogin(IsNotAuthenticated, FormView):
    form_class = forms.UserLoginForm
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('core:home')

    def form_valid(self, form):
        cd = form.cleaned_data
        try:
            user = authenticate(self.request, username=cd['username'], password=cd['password'])
            login(self.request, user)
            messages.success(self.request, 'you logged in successfuly', 'info')
        except:
            messages.success(self.request, 'username or password worng', 'danger')
            return render(self.request, self.template_name, {'form': self.form_class})
        return super().form_valid(form)


class UserLogout(LoginRequiredMixin, auth_view.LogoutView):
    next_page = 'accounts:login'


class UserDashboard(LoginRequiredMixin, FormView, ListView):
    USER = get_user_model()
    template_name = 'accounts/dashboard.html'
    context_object_name = 'user'
    form_class = forms.UserAvatarForm
    success_url = reverse_lazy('accounts:dashboard')

    def get_queryset(self):
        user = self.USER.objects.get(username=self.request.user)
        return user
    
    # update user avatar
    def post(self, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            cd = form.cleaned_data
            user = self.USER.objects.get(username=self.request.user)
            user.avatar = cd['avatar']
            user.save()
            messages.success(self.request, 'your avatar update', 'success')
        return super().form_valid(form)
from django.test import SimpleTestCase, TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings
from accounts import forms


class TestForm(SimpleTestCase):
    def test_user_registration_form(self):
        form = forms.UserRegistrationForm(
            data={
                'username': 'mahdi',
                'email': 'mahdi@gmail.com',
                'password': 'qwertyyrwe',
                }
            )
        self.assertTrue(form.is_valid())

    def test_user_login_form(self):
        form = forms.UserLoginForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 2)

    def test_user_avatar_form(self):
        file = open(f"{settings.BASE_DIR}/accounts/test/sepahbod.jpeg", 'br')
        upload = SimpleUploadedFile(file.name, file.read())
        form = forms.UserAvatarForm(data={'avatar': upload})
        print(form.errors)
        # self.assertTrue(form.is_valid())


class TestCustomUserForm(TestCase):
    def test_custom_user_creation_form(self):
        form = forms.CustomUserCreationForm(
            data={
                'email': 'mahdi@gmail.com',
                'password1': '12qwertyuio',
                'password2': '12qwertyuio',
                }
            )
        self.assertTrue(form.is_valid())

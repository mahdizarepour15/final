from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from accounts import views, forms, models


class TestUserRegisterView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_user_registrer_GET(self):
        response = self.client.get(reverse('accounts:register'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/register.html')

    def test_user_registrer_POST_valid(self):
        response = self.client.post(
            reverse('accounts:register'),
            data={
                'username': 'mahdi',
                'email': 'mahdi@gmail.com',
                'password': 'qwertyuio12',
                }
            )
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('accounts:dashboard'), 302, 302)
        self.assertEqual(get_user_model().objects.count(), 1)

    def test_user_registrer_POST_invalid(self):
        response = self.client.post(
            reverse('accounts:register'),
            data={
                'username': 'mahdi',
                'email': 'invalid-email',
                'password': 'qwertyuio12',
                }
            )
        self.assertEqual(response.status_code, 200)
        self.failIf(response.context['form'].is_valid())
        self.assertFormError(response, 'form', 'email', ['Enter a valid email address.'])
        self.assertEqual(get_user_model().objects.count(), 0)

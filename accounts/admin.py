from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = (
        'id',
        '__str__',
        'is_active',
        'is_staff',
        'is_superuser',
        'date_joined',
    )
    list_filter = (
        'email',
        'is_active',
        'is_staff',
        'is_superuser',
    )
    fieldsets = (
        (None,
         {'fields': (
             'email',
             'password',
             'username',
             'first_name',
             'last_name',
             'avatar',
                ),
            },
         ),
        ('Permissions',
         {'fields': (
             'is_active',
             'is_staff',
             'is_superuser',
             'groups',
                ),
            },
         ),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'password1',
                'password2',
                'is_active',
                'is_staff',
                'is_superuser',
                )
            }
         ),
    )
    search_fields = (
        'is_staff',
        'is_superuser',
        )
    ordering = (
        'username',
    )


admin.site.register(CustomUser, CustomUserAdmin)
from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    def create_user(self, username, email, password):
        if not username:
            raise ValueError('users must have username')

        # self.model: User Model
        user = self.model(
           username=username,
           email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_normal_user(self, username,  email, password):
        user = self.create_user(username, email, password)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username, email, password)
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user